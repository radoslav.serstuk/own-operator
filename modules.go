package main

type ErrorResponse struct {
	Error string `json:"error"`
}

type Versions struct {
	Version   string `json:"version"`
	Commit    string `json:"commit"`
	BuildTime string `json:"buildTime"`
}

type Book struct {
	Authors []Author `json:"authors"`
}

type Author struct {
	Key string `json:"key"`
}

type Name struct {
	Name string `json:"personal_name"`
}

type RespAuthor struct {
	Name string `json:"name"`
	Key  string `json:"authorKey"`
}

type Entries struct {
	Entries []Books `json:"entries"`
}
type Books struct {
	Title    string `json:"title"`
	Key      string `json:"key"`
	Revision int    `json:"revision"`
	Created  struct {
		Value string `json:"value"`
	} `json:"created"`
}
type RespBooks struct {
	Name        string `json:"name"`
	Key         string `json:"key"`
	Revision    int    `json:"revision"`
	PublishDate string `json:"publishDate"`
}

type Config struct {
	Server      *ServerConfig      `validate:"required"`
	OpenLibrary *OpenLibraryConfig `validate:"required"`
	Censors     []string           `validate:"dive,author"`
}

type ServerConfig struct {
	Addr       string `validate:"required,hostname_port"`
	BaseApiUrl string `validate:"required,startswith=/,endsnotwith=/,excludes=//"`
	SwaggerUrl string `validate:"required,startswith=/,endsnotwith=/,excludes=//"`
	MetricsUrl string `validate:"required,startswith=/,endsnotwith=/,excludes=//"`
}

type OpenLibraryConfig struct {
	BaseUrl     string `validate:"required,fqdn"`
	ResultLimit uint16 `validate:"required,gte=1,lte=1000"`
}
